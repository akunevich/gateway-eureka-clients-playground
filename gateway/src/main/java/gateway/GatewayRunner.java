package gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication(scanBasePackages = {"gateway", "io.github.resilience4j", "io.github.resilience4j.circuitbreaker"})
public class GatewayRunner {

    public static void main(String[] args) {
        SpringApplication.run(GatewayRunner.class);
    }
}
